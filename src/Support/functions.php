<?php

use Hypemedia\Schaltbau\Translation\Translate;

if(!function_exists("trans")) {
    function trans($key) {

        $segments = explode('.', $key);
        array_unshift($segments, "theme");

        $key = array_pop($segments);
        $domain = implode('.', $segments);

        return Translate::key($key, $domain);
    }
} else {
    throw new RuntimeException("trans() function already defined!");
}