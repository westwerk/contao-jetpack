<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 25/08/15
 * Time: 14:40
 */

namespace Hypemedia\Schaltbau\Translation;

use ContaoCommunityAlliance\Translator\StaticTranslator;
use ContaoCommunityAlliance\Translator\TranslatorInterface;

/**
 * Class Translate
 *
 * @package Hypemedia\Schaltbau\Translation
 */
class Translate
{

    /**
     * @var null|TranslatorInterface
     */
    private static $cachedTranslator = null;

    /**
     * @var string
     */
    private static $locale = 'en';

    /**
     * @param $key
     * @return string
     */
    public static function key($key = 'yes', $domain = 'theme')
    {
        if (self::$cachedTranslator === null) self::bootstrap();

        return self::getTranslation($key, $domain, self::$locale);
    }

    /**
     * @return string
     */
    protected static function getLanguageFilePath()
    {
        return TL_ROOT . '/resources/languages.php';
    }

    /**
     * @param $key
     * @param $domain
     * @return string
     */
    private static function getDefault($key, $domain)
    {
        if (self::$cachedTranslator === null) self::bootstrap();

        return self::getTranslation($key, $domain, 'en');
    }

    /**
     * @param $key
     * @param $domain
     * @param string $locale
     * @return string
     */
    private static function getTranslation($key, $domain, $locale = 'en')
    {
        if (self::$cachedTranslator === null) self::bootstrap();

        if (($trans = self::$cachedTranslator->translate($key, $domain, [], $locale)) != $key) {
            return $trans;
        } else {
            return self::getDefault($key, $domain);
        }
    }

    /**
     * @return void
     */
    private static function bootstrap()
    {
        $languageFilePath = self::getLanguageFilePath();

        if(!file_exists($languageFilePath)) {
            throw new \RuntimeException("Lanugage file missing under " . $languageFilePath);
        }

        self::setLocaleFromGlobals();
        $translator = new StaticTranslator();
        $translations = require($languageFilePath);
        self::readTranslations($translations, $translator);
        self::$cachedTranslator = $translator;
    }

    /**
     * @return void
     */
    private static function setLocaleFromGlobals()
    {
        self::$locale = $GLOBALS['TL_LANGUAGE'];
    }

    /**
     * @param $translations
     * @param StaticTranslator $translator
     * @param string $domain
     * @return void
     */
    private static function readTranslations($translations, &$translator, $domain = 'theme')
    {
        foreach ($translations as $key => $translation) {
            if (self::isDomain($translation)) {
                $newDomain = sprintf("%s.%s", $domain, $key);
                self::readTranslations($translation, $translator, $newDomain);
            } else {
                foreach ($translation as $locale => $value) {
                    $translator->setValue($key, $value, $domain, $locale);
                }
            }
        }
    }

    /**
     * @param array $array
     * @return bools
     */
    private static function isDomain(array $array)
    {
        return !isset($array['en']);
    }

}