<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 25/08/15
 * Time: 16:23
 */

namespace Hypemedia\Contao\Pageslider\Inserttag;


trait CacheableTrait
{

    /**
     * @var array
     */
    protected $cache = [];

    /**
     * @param string $fullTag
     */
    protected function isCached($fullTag)
    {
        return isset($this->cache[ $fullTag ]);
    }

    /**
     * @param string $fullTag
     */
    protected function getCached($fullTag)
    {
        return $this->isCached($fullTag) ? $this->cache[ $fullTag ] : null;
    }

    /**
     * @param string $fullTag
     * @param string $output
     * @return $this
     */
    protected function addToCache($fullTag, $output)
    {
        $this->cache[ $fullTag ] = $output;

        return $this;
    }

}