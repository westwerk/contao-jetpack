<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 17/06/15
 * Time: 12:38
 */

namespace Hypemedia\Contao\Pageslider\Inserttag;


use Contao\Backend;

abstract class Tag extends Backend implements TagContract
{

    /**
     * This tag class' tag name
     *
     * @var string
     */
    protected $tagName;

    /**
     * Checks if the concrete tag class
     * actually should respond to the given tag.
     *
     * @param string $tag
     * @return bool
     */
    protected function shouldRespondTo($tag)
    {
        return $this->getTagName() === $tag;
    }

}