<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 17/06/15
 * Time: 12:35
 */

namespace Hypemedia\Contao\Pageslider\Inserttag;

/**
 * Interface Tag
 * @package Hypemedia\Contao\Pageslider\Inserttag
 */
interface TagContract {

    /**
     * Returns a string in the form of "insert::tag".
     *
     * @return string
     */
    public function getTagName();

    /**
     * Chainable setter for the tag name.
     *
     * @param $tagName
     * @return $this
     */
    public function setTagName($tagName);

    /**
     * Entrypoint function used to register with hooks.
     *
     * @param string $tag
     * @return mixed
     */
    public function handle($tag);

}